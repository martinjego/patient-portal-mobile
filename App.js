import React from 'react';
import {SafeAreaView, StyleSheet, View, Dimensions} from 'react-native';
import {OTSession, OTPublisher, OTSubscriber} from 'opentok-react-native';

const App = () => {
  const apiKey = '45828062';
  const sessionId =
    '2_MX40NTgyODA2Mn5-MTU3NTkwNzc1NzIzOX5PbXJ1bkVCb0xTVGthdUhveEx5ankrK2x-UH4';
  const token =
    'T1==cGFydG5lcl9pZD00NTgyODA2MiZzaWc9N2NjYzBiZDBjODc4NGNkY2Q3ZDMyYzg5N2RjYmMxMDI5OTdhNzU1ZjpzZXNzaW9uX2lkPTJfTVg0ME5UZ3lPREEyTW41LU1UVTNOVGt3TnpjMU56SXpPWDVQYlhKMWJrVkNiMHhUVkd0aGRVaHZlRXg1YW5rcksyeC1VSDQmY3JlYXRlX3RpbWU9MTU3NTkwNzg0MyZub25jZT0wLjU5MDMxMDAyNzQyNzM2JnJvbGU9cHVibGlzaGVyJmV4cGlyZV90aW1lPTE1NzU5OTQyNDM=';

  return (
    <SafeAreaView style={styles.container}>
      <OTSession apiKey={apiKey} sessionId={sessionId} token={token}>
        <View style={styles.container}>
          <OTPublisher
            style={styles.cameraStyle}
            properties={{publishAudio: false}}
          />
          <OTSubscriber style={styles.cameraStyle} />
        </View>
      </OTSession>
    </SafeAreaView>
  );
};

const {width, height} = Dimensions.get('window');
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  cameraStyle: {
    width: width,
    height: height / 3,
  },
});

export default App;
